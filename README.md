# Sealed Sealed Deployment

## Requirements

- [Kubectl Krew](https://krew.sigs.k8s.io/) - Krew is the plugin manager for kubectl command-line tool.
- [Kubectl Slice](https://github.com/patrickdappollonio/kubectl-slice) - kubectl-slice allows you to split a single multi-YAML Kubernetes manifest into multiple subfiles.
- [Kubeseal](https://github.com/bitnami-labs/sealed-secrets?tab=readme-ov-file#kubeseal) - kubeseal utility uses asymmetric crypto to encrypt secrets that only the controller can decrypt.

## Deploying

1. Download [controller.yaml](https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.26.2/controller.yaml) from latest release:

```sh
wget https://github.com/bitnami-labs/sealed-secrets/releases/download/v0.26.2/controller.yaml
```

2. Slice `controller.yaml`:

```sh
kubectl-slice --input-file controller.yaml --output-dir ./bases/ --template "sealed-secrets.{{.kind |lower}}.yaml"
```

3. Add manifests to resource list in `bases/kustomization.yaml`:

```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
  - sealed-secrets.clusterrole.yaml
  - sealed-secrets.clusterrolebinding.yaml
  - sealed-secrets.customresourcedefinition.yaml
  - sealed-secrets.deployment.yaml
  - sealed-secrets.role.yaml
  - sealed-secrets.rolebinding.yaml
  - sealed-secrets.service.yaml
  - sealed-secrets.serviceaccount.yaml
```

4. Create keypair:

```sh
openssl req -x509 -days 365 -nodes -newkey rsa:4096 -keyout environments/prod/bases/certs/tls.key -out environments/prod/bases/certs/tls.crt -subj "/CN=sealed-secret/O=sealed-secret"
```

5. Create tls secret:

```sh
kubectl create secret tls sealed-secrets-certs --cert environments/prod/bases/certs/tls.crt --key environments/prod/bases/certs/tls.key --output yaml --dry-run=client > sealed-secrets-certs.secret.yaml
```

6. Deploy sealed-secrets to k8s:

```sh
kubectl apply -k environments/prod/
```

## Repository Structure

The repository is organized as follows:

```sh
├── bases
│   ├── kustomization.yaml
│   ├── sealed-secrets.clusterrole.yaml
│   ├── sealed-secrets.clusterrolebinding.yaml
│   ├── sealed-secrets.customresourcedefinition.yaml
│   ├── sealed-secrets.deployment.yaml
│   ├── sealed-secrets.role.yaml
│   ├── sealed-secrets.rolebinding.yaml
│   ├── sealed-secrets.service.yaml
│   └── sealed-secrets.serviceaccount.yaml
└── environments
    └── prod
        ├── bases
        │   └── certs
        │       ├── tls.crt
        │       └── tls.key
        ├── kustomization.yaml
        └── sealed-secrets-certs.secret.yaml
```

## Sealing Secrets

### Usage

```sh
# Create a json/yaml-encoded Secret somehow:
# (note use of `--dry-run` - this is just a local file!)
echo -n bar | kubectl create secret generic mysecret --dry-run=client --from-file=foo=/dev/stdin -o json > mysecret.json

# This is the important bit:
kubeseal -f mysecret.json -w mysealedsecret.json

# At this point mysealedsecret.json is safe to upload to Github,
# post on Twitter, etc.

# Eventually:
kubectl create -f mysealedsecret.json

# Profit!
kubectl get secret mysecret
```

### Validate

```sh
cat <sealed-secret> | kubeseal --controller-namespace ops --validate
```

### Certs

**Expires:** May 12 15:56:00 2025 GMT

```
-----BEGIN CERTIFICATE-----
MIIFQTCCAymgAwIBAgIUWmmXddsZ+LLv9OboE1S/rH38es4wDQYJKoZIhvcNAQEL
BQAwMDEWMBQGA1UEAwwNc2VhbGVkLXNlY3JldDEWMBQGA1UECgwNc2VhbGVkLXNl
Y3JldDAeFw0yNDA1MTIxNTU2MDBaFw0yNTA1MTIxNTU2MDBaMDAxFjAUBgNVBAMM
DXNlYWxlZC1zZWNyZXQxFjAUBgNVBAoMDXNlYWxlZC1zZWNyZXQwggIiMA0GCSqG
SIb3DQEBAQUAA4ICDwAwggIKAoICAQCriWVDrT5o6NDTG/62tbN2ccJaRTs2YBIY
w1JN5jVfEAEGzKRvu9fpaX75x9LOnr6vwAV86Q70oZ08sesq7qjVTCLBtxrWhK93
39E+2MdPfl2/9yfzfvFjY2cNFoImCb3E0X4f4Ggr+MxzlE2hviUKj0LqPgUkwqiD
3gEp7awiOrFFycpFLz26hBbPhYeqh8irS1H9W5IpGMwjub2D5yONVtU87tUsbIDv
v8GMbMU7hSz04St3wNygm+mce2NKpgCF9wzzibRsWTj82/dh9PYk0FcpQiPsChAw
jIm+pOUpAatwrtOFeoA71/cY7YxS6WfBMpGc2qaq5mjQLpH6GPV07BlIfaT4ANWK
VxdQjORwFPbU6SjzuuhVZmx2p3o0kw823e0tJettAseuQX5yLSjQAyyIilmaB+oe
tC2z9MS3bxp7klf7ShvouSu4UClXHlqvufdo0IjJ/JNQGo0r/7Kz1BGQK5amJr79
0khn++iz3TtU349BoB2kLHN15jEqAwujUIrT+yxa5CmfEQs88FsDLwmkeNwTZ7Dm
KWjUfSe5Wa/gDaTOvwZ0ndkyz53cAIoGijO9Uk4H8R+kye1OkujM+cICNsZX0G1T
kpm1O/tvQv+xhUYAjejKTfakEn5NChYjFg+qVFePFJOSu0SHKCGqnJ/ij12DOUnW
P3zS6AUt3wIDAQABo1MwUTAdBgNVHQ4EFgQUnEKO/vb9cpdTm6IfjgPQSph3kCsw
HwYDVR0jBBgwFoAUnEKO/vb9cpdTm6IfjgPQSph3kCswDwYDVR0TAQH/BAUwAwEB
/zANBgkqhkiG9w0BAQsFAAOCAgEAQF1enkA6g0PxeT2N91uKPO8wBAMjdYHuXAVl
UBAZAMCcQOraSvkoa0/SmB3P7I6e/spJrLFvugdBRjGsFgvDV3nU1BwIdWCv0nz1
bisf3SJt3oXXMc29dunAbxDItC/1hHYWNS32H9tLwWqMXMCPeS0Bxveef3NVNgU2
Ua88UoMTeEJj7a514fdZ0z4OXsD1nF5EkSuqSOZhhfjGI2NhIozJkh4P2oJ44SpU
Kde6huO4b6yJu6bGqI640r7lAR75dKcas/fEZwXgkJ4tVSyp5Ua3DiJVhzHIsO0n
Gp2BfmUlFgH6UUd4zQ+lvsYB/rbvJr43FXPAwlUxy5sPTi5of9EmbC8qwzKTFF6w
q8Tqrfc7Q2tpZZ25KUa1/Yc1o+uBWJ5d4l3N5Vyn5/EHxnKyucEZZth2y+2YW4cb
8dP4vcR7m+034uk0L1+FicvT1waCJX8hqM3lC8QFZaV+fgCBNNpd1Yx4+PrdBE9o
JQ5v7Nq9nW/dkTCHGRoZoqG9jtGHdiji7df19qtsrzYzKBLZp/sX2cRsB7CMd0ug
cRQojOXN5q0p87gjA4wZ6ZGripPehuFR80Jwt+34DW01KSQs/dqaIw1d6OiGQX3P
TNSoXyxs+EmKSg8yu8dn0BaN9EZjHD8UQX+oc6IA1YC+yYmGUGkVJIWDMF7WM4fp
ucobKuE=
-----END CERTIFICATE-----
```
